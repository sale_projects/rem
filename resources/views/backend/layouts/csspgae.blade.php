<link rel="apple-touch-icon" href="{{ asset('backend/app-assets/images/favicon/apple-touch-icon-152x152.png') }}">
<link rel=" shortcut icon" type="image/x-icon"
	  href="{{ asset('backend/app-assets/images/favicon/favicon-32x32.png') }}">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- BEGIN: VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('backend/app-assets/vendors/vendors.min.css') }}">
<!-- END: VENDOR CSS-->
<!-- BEGIN: Page Level CSS-->
<link rel="stylesheet" type="text/css"
	  href="{{ asset('backend/app-assets/css/themes/vertical-dark-menu-template/materialize.min.css') }}">
<link rel="stylesheet" type="text/css"
	  href="{{ asset('backend/app-assets/css/themes/vertical-dark-menu-template/style.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/app-assets/css/pages/dashboard.min.css') }}">
<!-- END: Page Level CSS-->
<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('backend/app-assets/css/custom/custom.css') }}">
<!-- END: Custom CSS-->
