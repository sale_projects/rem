<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
	@yield('title')

	@include('backend.layouts.metapage')

	@include('backend.layouts.csspgae')

	@yield('CSS')
</head>
<!-- END: Head-->
<body
	class="vertical-layout page-header-light vertical-menu-collapsible vertical-dark-menu preload-transitions 2-columns   "
	data-open="click" data-menu="vertical-dark-menu" data-col="2-columns">

	@include('backend.layouts.header')

	@include('backend.layouts.sidenavmain')

	@yield('content')

	@include('backend.layouts.themecustomizer')

	@include('backend.layouts.footer')

	@yield('js')
</body>
</html>
