<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark sidenav-active-rounded">
	<div class="brand-sidebar">
		<h1 class="logo-wrapper"><a class="brand-logo darken-1" href="#">
				<img class="hide-on-med-and-down " src="{{ asset('backend/app-assets/images/logo/materialize-logo.png') }}" alt="rem admin"/>
				<img class="show-on-medium-and-down hide-on-med-and-up" src="{{ asset('backend/app-assets/images/logo/materialize-logo-color.png') }}" alt="rem admin"/>
				<span class="logo-text hide-on-med-and-down">REM Admin</span></a>
				<a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a>
		</h1>
	</div>
	<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">
		<li class="active bold">
			<a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)">
				<i class="material-icons">settings_input_svideo</i>
				<span class="menu-title" data-i18n="Dashboard">Dashboard</span>
				<span class="badge badge pill orange float-right mr-10">3</span>
			</a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">
					<li class="active">
						<a class="active" href="#">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="eCommerce">Dashboard</span>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="Modern">Products</span>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="Modern">Category</span>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="Modern">Blogs</span>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="Modern">Pgae</span>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="Modern">Settings</span>
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li class="navigation-header">
			<a class="navigation-header-text">Blogs</a>
			<i class="navigation-header-icon material-icons">more_horiz</i>
		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)">
				<i class="material-icons">photo_filter</i>
				<span class="menu-title" data-i18n="Menu levels">Posts</span>
			</a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">
					<li>
						<a href="JavaScript:void(0)">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="Second level">New Add</span>
						</a>
					</li>
					<li>
						<a href="JavaScript:void(0)">
							<i class="material-icons">radio_button_unchecked</i>
							<span data-i18n="Second level">View List</span>
						</a>
					</li>
				</ul>
			</div>
		</li>
        <li class="bold">
            <a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)">
                <i class="material-icons">photo_filter</i>
                <span class="menu-title" data-i18n="Menu levels">Category</span>
            </a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li>
                        <a href="JavaScript:void(0)">
                            <i class="material-icons">radio_button_unchecked</i>
                            <span data-i18n="Second level">New Add</span>
                        </a>
                    </li>
                    <li>
                        <a href="JavaScript:void(0)">
                            <i class="material-icons">radio_button_unchecked</i>
                            <span data-i18n="Second level">View List</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="bold">
            <a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)">
                <i class="material-icons">photo_filter</i>
                <span class="menu-title" data-i18n="Menu levels">Tags</span>
            </a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li>
                        <a href="JavaScript:void(0)">
                            <i class="material-icons">radio_button_unchecked</i>
                            <span data-i18n="Second level">New Add</span>
                        </a>
                    </li>
                    <li>
                        <a href="JavaScript:void(0)">
                            <i class="material-icons">radio_button_unchecked</i>
                            <span data-i18n="Second level">View List</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

{{--        Ecommerce--}}
{{--         Report--}}
{{--         order--}}
{{--         product--}}
{{--         product tags--}}
{{--         product categpry--}}
{{--         product attributes--}}
{{--         product labels--}}
{{--         brands--}}
{{--         flash sales--}}
{{--         Product collections--}}
{{--         review--}}
{{--         shipping--}}
{{--         discount--}}
{{--         shipping--}}
{{--         customer--}}
{{--         taxes--}}
{{--         settings--}}
{{--        Simple sliders--}}
{{--        Contact--}}
{{--        Media--}}
{{--        Settings--}}
{{--            general--}}
{{--            email--}}
{{--            media--}}
{{--            pemalink--}}
{{--            social login--}}
{{--        Appearance--}}
{{--            theme--}}
{{--            menu--}}
{{--            theme settings--}}
{{--            custome css--}}
{{--            custome js--}}
{{--            Widgets--}}
    </ul>
	<div class="navigation-background"></div>
	<a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out">
		<i class="material-icons">menu</i>
	</a>
</aside>
<!-- END: SideNav-->
