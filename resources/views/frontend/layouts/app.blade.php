<!DOCTYPE html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	@yield('title')

	@include('frontend.layouts.metapage')

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('frontend/assets/imgs/theme/favicon.svg') }}">

	@include('frontend.layouts.csspgae')

	@yield('CSS')
</head>

<body>
	@include('frontend.layouts.quickViewModal')

	@include('frontend.layouts.header')

	@yield('content')

	@include('frontend.layouts.footer')

	@include('frontend.layouts.preloader')

	@include('frontend.layouts.jspage')

	@yield('js')
</body>

</html>
