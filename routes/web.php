<?php

	use Illuminate\Support\Facades\Route;

	Route::get('/', function () {
		return redirect(app()->getLocale());
	});

	Route::group([
		'prefix' => '{locale}',
		'where' => ['locale' => '[a-zA-Z]{2}'],
		'middleware' => 'setlocale'
	], function () {
		//Home Page - shop
		Route::get('/', [App\Http\Controllers\HomePageController::class, 'index'])->name('shop.index');

		//Blogs
		Route::get('/blogs', [App\Http\Controllers\BlogsController::class, 'index'])->name('blogs.index');

		//Products
		Route::get('/products', [App\Http\Controllers\ProductsController::class, 'index'])->name('products.index');

		//Category
		Route::get('/category', [App\Http\Controllers\CategoryController::class, 'index'])->name('category.index');

		//contact
		Route::get('/contact', [App\Http\Controllers\ContactController::class, 'index'])->name('contact.index');

		//about us
		Route::get('/aboutus', [App\Http\Controllers\AboutUsController::class, 'index'])->name('aboutus.index');

		//account
		Route::get('/account', [App\Http\Controllers\AccountController::class, 'index'])->name('account.index');

		//admin
		Route::get('/admin', [App\Http\Controllers\Admin\AdminController::class, 'index'])->name('admin.index');


		Auth::routes();

		Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	});


